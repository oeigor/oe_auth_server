/*
 * Copyright 2012 SURFnet bv, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.surfnet.oaaas.auth; 

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.surfnet.oaaas.auth.OAuth2Validator.ValidationResponse;
import org.surfnet.oaaas.model.AuthorizationRequest;
import org.surfnet.oaaas.repository.AuthorizationRequestRepository;

@Named
public class AuthenticationFilter implements Filter {
	
  public static String PARAM_OLD_PROFILE_ID = "old_profile_id";

  private static final Logger LOG = LoggerFactory.getLogger(AuthenticationFilter.class);

  private AbstractAuthenticator authenticator;

  @Inject
  private AuthorizationRequestRepository authorizationRequestRepository;

  @Inject
  private OAuth2Validator oAuth2Validator;

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    /*
     * Create an authorizationRequest from the request parameters.
     * This can be either a valid or an invalid request, which will be determined by the oAuth2Validator.
     */
    AuthorizationRequest authorizationRequest = extractAuthorizationRequest(request);
    final ValidationResponse validationResponse = oAuth2Validator.validate(authorizationRequest);

    if (authenticator.canCommence(request)) {
      /*
      * Ok, the authenticator wants to have control again (because he stepped
      * out)
      */
      if (request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("forgot_password")) {
    	  if (req.getAttribute("apiResponseCode") == null) {
    		  // success case
    		  AuthorizationRequest aReq = findAuthorizationRequest(request);
    		  if (aReq != null && aReq.getRedirectUri() != null) {
    			  response.setHeader("redirect", aReq.getRedirectUri());
    		  }
    	  }
    	  // failure case, we just display message with ajax
    	  return;
      }
      authenticator.doFilter(request, response, chain);
    } else if (validationResponse.valid()) {
      // Request contains correct parameters to be a real OAuth2 request.
      handleInitialRequest(authorizationRequest, request);
      authenticator.doFilter(request, response, chain);
    } else {
      // not an initial request but authentication module cannot handle it either
      String errorMessage = null;
      if (request.getAttribute("apiResponse") != null) {
    	  errorMessage = request.getAttribute("apiResponse").toString();
      }
      if (errorMessage == null && validationResponse != null) {
    	  errorMessage = validationResponse.getValue() + ". " + validationResponse.getDescription();
      }
      if (errorMessage == null) {
    	  errorMessage = "Error during request handling";
      }
      response.sendError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), errorMessage);
    }
  }

  protected AuthorizationRequest extractAuthorizationRequest(HttpServletRequest request) {
    String responseType = request.getParameter("response_type");
    String clientId = request.getParameter("client_id");
    String redirectUri = request.getParameter("redirect_uri");

    List<String> requestedScopes = null;
    if (StringUtils.isNotBlank(request.getParameter("scope"))) {
      requestedScopes = Arrays.asList(request.getParameter("scope").split(","));
    }

    String state = request.getParameter("state");
    String authState = getAuthStateValue();

    return new AuthorizationRequest(responseType, clientId, redirectUri, requestedScopes, state, authState);
  }

  private boolean handleInitialRequest(AuthorizationRequest authReq, HttpServletRequest request) throws
      ServletException {

      try {
        authorizationRequestRepository.save(authReq);
      } catch (Exception e) {
        LOG.error("while saving authorization request", e);
        throw new ServletException("Cannot save authorization request");
      }

      request.setAttribute(AbstractAuthenticator.AUTH_STATE, authReq.getAuthState());
      request.setAttribute(AbstractAuthenticator.RETURN_URI, request.getRequestURI());
      request.setAttribute(PARAM_OLD_PROFILE_ID, request.getParameter(PARAM_OLD_PROFILE_ID));
      return true;
  }
  
  private AuthorizationRequest findAuthorizationRequest(HttpServletRequest request) {
	  String authState = (String) request.getAttribute(AbstractAuthenticator.AUTH_STATE);
	  if (StringUtils.isBlank(authState)) {
		  authState = request.getParameter(AbstractAuthenticator.AUTH_STATE);
	  }
	  return authorizationRequestRepository.findByAuthState(authState);
  }

  protected String getAuthStateValue() {
    return UUID.randomUUID().toString();
  }

  private void sendError(HttpServletRequest request, HttpServletResponse response, AuthorizationRequest authReq, ValidationResponse validate)
      throws IOException {
    LOG.info("Will send error response for authorization request '{}', validation result: {}", authReq, validate);
    String redirectUri = authReq.getRedirectUri();
    AuthorizationRequest authorizationRequest = findAuthorizationRequest(request);
    // try to retrieve redirect_uri from database
    if (redirectUri == null && authorizationRequest != null) {
    	redirectUri = authorizationRequest.getRedirectUri();
    }
    // try to retrieve redirect_uri from client's list 
    if (redirectUri == null) {
    	try {
    		redirectUri = ((OAuth2ValidatorImpl)oAuth2Validator).determineRedirectUri(authReq, authReq.getResponseType(), authReq.getClient());
    	} catch (Exception e) {
    		// ignore
    	}
    }
    String state = authReq.getState();
    if (isValidUrl(redirectUri)) {
      redirectUri = redirectUri.concat(redirectUri.contains("?") ? "&" : "?");
      redirectUri = redirectUri
              .concat("error=").concat(request.getAttribute("apiResponseCode") != null ? request.getAttribute("apiResponseCode").toString() : validate.getValue())
              .concat("&error_description=").concat(request.getAttribute("apiResponse") != null ? request.getAttribute("apiResponse").toString() : validate.getDescription())
              .concat(StringUtils.isBlank(state) ? "" : "&state=".concat(URLEncoder.encode(state, "UTF-8")));
      LOG.info("Sending error response, a redirect to: {}", redirectUri);
      response.sendRedirect(redirectUri);
    } else {
      LOG.info("Sending error response 'bad request': {}", validate.getDescription());
      response.sendError(HttpServletResponse.SC_BAD_REQUEST, validate.getDescription());
    }
  }
  
  @Override
  public void destroy() {
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  public void setAuthenticator(AbstractAuthenticator authenticator) {
    this.authenticator = authenticator;
  }

  public static boolean isValidUrl(String redirectUri) {
    try {
      new URL(redirectUri);
      return true;
    } catch (MalformedURLException e) {
      return false;
    }
  }

  /**
   * @param authorizationRequestRepository
   *          the authorizationRequestRepository to set
   */
  public void setAuthorizationRequestRepository(AuthorizationRequestRepository authorizationRequestRepository) {
    this.authorizationRequestRepository = authorizationRequestRepository;
  }

  /**
   * @param oAuth2Validator
   *          the oAuth2Validator to set
   */
  public void setOAuth2Validator(OAuth2Validator oAuth2Validator) {
    this.oAuth2Validator = oAuth2Validator;
  }

}
