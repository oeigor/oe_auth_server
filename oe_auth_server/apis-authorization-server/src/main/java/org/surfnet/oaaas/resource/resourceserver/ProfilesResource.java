package org.surfnet.oaaas.resource.resourceserver;

import javax.inject.Named;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * JAX-RS Resource for profile (stub implementation only!).
 */

@Named
@Path("/u/v1/transactions")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class ProfilesResource extends AbstractResource {

	private static final Logger LOG = LoggerFactory.getLogger(ProfilesResource.class);
	
	@POST
	@Path("/oauth/authenticate")
	@Produces(MediaType.APPLICATION_JSON)
	public String authenticate() {
		LOG.info("Authenticating authServer user...");
		return "{"
				+ "success: true; "
				+ "profile_id: admin; "
				+ "login: admin@objectedge.com; "
				+ "first_name: Igor; "
				+ "last_name: Abramovich"
				+ "}";

	}

	@PUT
	@Path("/changePassword")
	@Produces(MediaType.APPLICATION_JSON)
	/*
	 * 	
	 	SUCCESS="200";
		PROFILE_NOT_FOUND="101";
		NEW_PASSWORD_DOES_NOT_MATCH_PASSWORD_RULES="102";
		PASSWORD_CAN_NOT_BE_UPDATED="103";
		OLD_PASSWORD_DOES_NOT_MATCH="104";
		INTERNAL_ERROR="150";
	 */
	
	public String changePassword() {
		LOG.info("Changing password for authServer user...");
		return "{updateStatus: 200}";
		
		
	}

	@POST
	@Path("/oauth/register")
	@Produces(MediaType.APPLICATION_JSON)
	
	/*
	 * HTTP Error codes: 422
	 */
	
	public String register() {
		LOG.info("Registering authServer user...");
		return "{"
		+ "success: true; "
		+ "profile_id: admin; "
		+ "message: profileCreated"
		+ "}";

	}

	@POST
	@Path("/forgot-password")
	@Produces(MediaType.APPLICATION_JSON)
	
	/*
	 * HTTP Error codes: 422
	 */
	
	public String forgotPassword() {
		LOG.info("Handling forgot password for authServer user...");
		return "{"
		+ "success: true; "
		+ "profile_id: admin; "
		+ "message: profileCreated"
		+ "}";

	}

}
