package com.objectedge.api;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.env.Environment;
import org.surfnet.oaaas.auth.AuthenticationFilter;
import org.surfnet.oaaas.auth.OAuth2Validator;
import org.surfnet.oaaas.model.AccessTokenResponse;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Implementation of {@link APIClient} that makes various api calls to ATG servers for scenarios like authentication, registration and password change.
 *
 */
@Named
public class OEAPIClientImpl implements OEAPIClient{
	
	private String PROP_SELF_CLIENT_ID = "selfClient.id";
	private String PROP_SELF_CLIENT_SECRET = "selfClient.secret";
	private String PROP_API_OLD_PROFILE_ID = "old_profile_id";
	private String PROP_API_LOGIN = "login";
	private String PROP_API_PASSWORD = "password";
	private String PROP_LOCAL_PORT = "local.port";

	private final Pattern REGEX_JSON_PASSWORD = Pattern.compile("\"(?i)(password)\":\"[\\w\\p{Punct}&&[^&]]*?\"");

	private ObjectMapper mapper = new ObjectMapper();
	private String cachedSelfClientToken = null; // token used by AuthServer as its own client
	@Inject
	Environment env;
	
	/* (non-Javadoc)
	 * @see com.surfstitch.api.APIClient#handleAuthentication(javax.servlet.http.HttpServletRequest, java.lang.String)
	 * 
	 */
	public JSONObject handleAuthentication(HttpServletRequest req, String url) throws IOException, JSONException{
		String login = req.getParameter("j_username");
		String password = req.getParameter("j_password");
		String oldProfileId = req.getParameter(AuthenticationFilter.PARAM_OLD_PROFILE_ID);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(PROP_API_LOGIN, login)
				  .put(PROP_API_PASSWORD, password)
				  .put(PROP_API_OLD_PROFILE_ID, oldProfileId);
		return invokeAPI(jsonObject, url, "post");		
	}
	
	/* (non-Javadoc)
	 * @see com.surfstitch.api.APIClient#handleChangePassword(javax.servlet.http.HttpServletRequest, java.lang.String)
	 */
	public JSONObject handleChangePassword(HttpServletRequest req, String url) throws IOException, JSONException{
		String login = req.getParameter("j_username");
		String oldPassword = req.getParameter("j_oldPassword");
		String newPassword = req.getParameter("j_newPassword");
		String confirmNewPassword = req.getParameter("j_confirmNewPassword");
		JSONObject jsonObject = new JSONObject();
		
		if(!newPassword.equals(confirmNewPassword)){
			jsonObject.put("error", "New password and confirm new password do not match");
			return jsonObject;
		}
		jsonObject.put("login", login)
				  .put("newPassword", newPassword)
				  .put("oldPassword", oldPassword);
		return invokeAPI(jsonObject, url, "put");
	}
	
	@Override
	public JSONObject handleForgotPassword(HttpServletRequest request, String url) throws Exception {
		String email = request.getParameter("email");
		JSONObject json = new JSONObject();
		return invokeAPI(json, MessageFormat.format("{0}?email={1}", url, email), "post");
	}

	
	/* (non-Javadoc)
	 * @see com.surfstitch.api.APIClient#handleProfieRegistration(javax.servlet.http.HttpServletRequest, java.lang.String)
	 */
	public JSONObject handleProfileRegistration(HttpServletRequest req, String url) throws Exception{
		String login = req.getParameter("j_username");
		String firstName = req.getParameter("j_firstName");
		String lastName = req.getParameter("j_lastName");
		String password = req.getParameter("j_password");
		String prefix = req.getParameter("j_prefix");
		String oldProfileId = req.getParameter(AuthenticationFilter.PARAM_OLD_PROFILE_ID);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("login", login)
				  .put("first_name", firstName)
				  .put("last_name", lastName)
				  .put("password", password)
				  .put("prefix", prefix)
				  .put(PROP_API_OLD_PROFILE_ID, oldProfileId);
				
		return invokeAPI(jsonObject, url, "post");
	}
	
	public static String authorizationBasic(String username, String password) {
		String concatted = username + ":" + password;
		return "Basic " + new String(Base64.encodeBase64(concatted.getBytes()));
	}
	
	public static String authorizationBearer(String token) {
	    return "Bearer " + token;
	}

	//curl -v -H "Accept: application/json" -H "Content-type: application/x-www-form-urlencoded" -H "Authorization: Basic YzE6ZjNiNjUxODAtNjIxOC00YjM4LWI0ZWItNzc2M2E2N2JhZTMy" -X POST -d 'grant_type=client_credentials' http://localhost:8080/oauth2/token
	
	/*
	 * Get a token to self client, i.e. when AuthServer itself is a client of the API. 
	 */
	
	private String getSelfClientCredentialToken (String username,
			String password) throws IOException {
		
		// String localHost = InetAddress.getLocalHost().getHostName(); (does not work on ecommerce VM)
		String localHost = "localhost";
		String localPort = getEnv().getProperty(PROP_LOCAL_PORT);
		
		String authServerUrl = String.format("http://%s:%s", localHost,
				localPort);

		String tokenUrl = String.format("%s/oauth2/token", authServerUrl);
		final HttpPost tokenRequest = new HttpPost(tokenUrl);
		String postBody = String.format("grant_type=%s",
				OAuth2Validator.GRANT_TYPE_CLIENT_CREDENTIALS);

		tokenRequest.setEntity(new ByteArrayEntity(postBody.getBytes()));
		tokenRequest.addHeader("Authorization",
				authorizationBasic(username, password));
		tokenRequest.addHeader("Content-Type",
				"application/x-www-form-urlencoded");

		HttpResponse tokenHttpResponse = new DefaultHttpClient()
				.execute(tokenRequest);
		
		InputStream responseContent = tokenHttpResponse.getEntity().getContent();
	    String content = IOUtils.toString(responseContent);	   
	    AccessTokenResponse accessTokenResponse =  mapper.readValue(content,AccessTokenResponse.class);
	    return accessTokenResponse.getAccessToken();
		
	}
	
	private String getCachedSelfClientToken() throws IOException{
		if (cachedSelfClientToken == null) {
			String selfClientKey = getEnv().getProperty(PROP_SELF_CLIENT_ID);
			String selfClientSecret = getEnv().getProperty(PROP_SELF_CLIENT_SECRET);
			cachedSelfClientToken = getSelfClientCredentialToken(selfClientKey, selfClientSecret);
		}
		return cachedSelfClientToken;
	}

	/**
	 * This method is used by the methods below to make the actualy api calls to the end server represented by the url parameter
	 * {@link #handleAuthentication(HttpServletRequest, String)}
	 * {@link #handleChangePassword(HttpServletRequest, String)}
	 * {@link #handleProfieRegistration(HttpServletRequest, String)}
	 * 
	 * @param jsonObject - input for the invocation of api
	 * @param url - end url of the ATG server
	 * @param operation - put, post, or get operation for the invocation of REST api
	 * 
	 * @return - response {@link JSONObject} after invocation of api
	 * @throws JSONException
	 * @throws IOException
	 */
	private JSONObject invokeAPI(JSONObject jsonObject, String url, String operation) throws JSONException, IOException{
		Client client = Client.create();
		WebResource postWebResource = client.resource(url);
		ClientResponse response = null;
		String accessToken = getCachedSelfClientToken();
		String logMessage = MessageFormat.format("About to invoke resourceServer URL: {0} {1} [token={2}] [json={3}]", operation, url, accessToken, jsonObject);
		logMessage =  REGEX_JSON_PASSWORD.matcher(logMessage).replaceAll("\"$1\":\"********\"");
		System.out.println(logMessage);
		if(operation.equals("put")){
			response = postWebResource.type(MediaType.APPLICATION_JSON).header("Authorization", authorizationBearer(accessToken)).put(ClientResponse.class, jsonObject.toString());
		}else if(operation.equals("post")){
			response = postWebResource.type(MediaType.APPLICATION_JSON).header("Authorization", authorizationBearer(accessToken)).post(ClientResponse.class, jsonObject.toString());
		}
		InputStream inputStream = response.getEntityInputStream();
		byte[] b = new byte[inputStream.available()];
		inputStream.read(b, 0, inputStream.available());
		String responseString = new String(b);
		System.out.println("About to parse responseString="+responseString);
		JSONObject responseJSON = new JSONObject(responseString);
		return responseJSON;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

}
