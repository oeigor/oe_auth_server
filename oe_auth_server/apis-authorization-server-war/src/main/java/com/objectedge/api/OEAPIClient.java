package com.objectedge.api;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

/**
 * Interface that has defines various apis which will be used by the auth server based on requests from client of the api server. This interface provides operations
 * that will and should not be handled through the client of the API such as user registration, password reset, authentication etc. The client will invoke the auth server for all such 
 * operations and the auth server will use this interface to make calls to the API server  
 * 
 * @author Object Edge Inc.
 *
 */
public interface OEAPIClient {

	/**
	 * This method invokes the registration api of ATG based on the registration info passed in the request.
	 *  
	 * @param req
	 * @param url - url of the ATG server
	 * @return - {@link JSONObject} of the response from API server
	 * @throws Exception
	 */
	public JSONObject handleProfileRegistration(HttpServletRequest req, String url) throws Exception;
	
	/**
	 * This method invokes the change password api of ATG based on the login and password passed in the request.
	 *  
	 * @param req
	 * @param url - url of the ATG server
	 * @return - {@link JSONObject} of the response from API server
	 * @throws Exception
	 */
	public JSONObject handleChangePassword(HttpServletRequest req, String url) throws Exception;
	
	/**
	 * This method invokes the authentication api of ATG based on the login and password passed in the request.
	 *  
	 * @param req
	 * @param url - url of the ATG server
	 * @return - {@link JSONObject} of the response from API server
	 * @throws Exception
	 */
	public JSONObject handleAuthentication(HttpServletRequest req, String url) throws Exception;

	/**
	 * This method invokes the forgot password service based on the email passed in the request.
	 *  
	 * @param req
	 * @param url - url of the ATG server
	 * @return - {@link JSONObject} of the response from API server
	 * @throws Exception
	 */
	public JSONObject handleForgotPassword(HttpServletRequest request, String url) throws Exception;
}
