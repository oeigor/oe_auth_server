package com.objectedge.oaaas.authentication;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.surfnet.oaaas.auth.AuthenticationFilter;
import org.surfnet.oaaas.auth.principal.AuthenticatedPrincipal;
import org.surfnet.oaaas.authentication.FormLoginAuthenticator;
import org.surfnet.oaaas.model.AccessToken;
import org.surfnet.oaaas.model.Client;
import org.surfnet.oaaas.model.ResourceServer;
import org.surfnet.oaaas.repository.AccessTokenRepository;
import org.surfnet.oaaas.repository.ClientRepository;

import com.objectedge.api.OEAPIClient;

/**
 * This class provides integration of OOAUTH login functionality with API server where the user gets authenticated.
 * It uses {@link APIClient} to authenticate the user against the API server.
 * 
 * @author Object Edge Inc.
 *
 */
public class LoginAuthenticator extends FormLoginAuthenticator {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginAuthenticator.class);
	private static final String SESSION_IDENTIFIER = "AUTHENTICATED_PRINCIPAL";
	private static final String PROFILE_NAME_IN_RESPONSE="name";
	private static final String API_RESPONSE="apiResponse";
	private static final String API_RESPONSE_CODE="apiResponseCode";
	
	private static final String AUTHORIZATION_PREFIX="/oauth2/authorize";
	private static final String PROP_ADMIN_USER_IDS = "adminUserIds";
	
	private static final String PARAM_ACTION = "action";
	private static final String PARAM_CLIENT_ID = "client_id";
	
	private static final String ACTION_REGISTER = "register";
	private static final String ACTION_FORGOT_PASSWORD = "forgot_password";
	
	private static final String ENV_PROP_RESOURCE_SERVER_ENDPOINTS = "resourceServerEndPoints";
	private static final String ENV_PROP_URL_LOGIN = "login_url";
	private static final String ENV_PROP_URL_REGISTER = "register_url";
	private static final String ENV_PROP_URL_FORGOT_PASSWORD = "forgot_password_url";
	
	@Inject
	private Environment env;

	@Inject
	private OEAPIClient apiClient;
	
	@Inject
	private AccessTokenRepository accessTokenRepository;
	
	@Inject
	private ClientRepository clientRepository;
	
	private String resolveResourceServerServiceEndpoint (HttpServletRequest request, String serviceName, String clientId) {
		String relativeServiceEndpoint = getEnv().getProperty(serviceName);
		Client client = clientRepository.findByClientId(clientId);
		ResourceServer resourceServer = client.getResourceServer();		
		String resourceServerHostPort = getResourseServerHostPort (resourceServer.getName());
		if (resourceServerHostPort == null) {
			LOGGER.info("resourceServerHostPort entry is not found for client in api.application.properties for " + resourceServer.getName());
			return null;
		}
		else {
			return resourceServerHostPort + relativeServiceEndpoint;
		}
	}
	
	private String getResourseServerHostPort (String resourceServerName) {
		try {
			String resourceServerEndpoints = getEnv().getProperty(ENV_PROP_RESOURCE_SERVER_ENDPOINTS);
			Properties resourceServerEndpointsAsProperties = new Properties();
			resourceServerEndpointsAsProperties.load(new StringReader(resourceServerEndpoints));
			LOGGER.info("Loaded resourceServerEndpointsAsProperties" + resourceServerEndpointsAsProperties.toString());
			return resourceServerEndpointsAsProperties.getProperty(resourceServerName);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.surfnet.oaaas.authentication.FormLoginAuthenticator#canCommence(javax.servlet.http.HttpServletRequest)
	 * 
	 * This method makes the call to resource server to handle authentication. See APIClient.handleAuthentication for details.
	 */
	@Override
	public boolean canCommence(HttpServletRequest request) {
		
		boolean canCommerce = super.canCommence(request);

		if(canCommerce){
			String apiEndPoint;
			String clientId = request.getParameter(PARAM_CLIENT_ID);
			try {
				JSONObject response = null;
				if(request.getRequestURI().equalsIgnoreCase(AUTHORIZATION_PREFIX)){
					if (ACTION_REGISTER.equalsIgnoreCase(request.getParameter(PARAM_ACTION))) {
						apiEndPoint = resolveResourceServerServiceEndpoint(request, ENV_PROP_URL_REGISTER, clientId);
						if (apiEndPoint == null) {
							LOGGER.info("No user registration end point for clientId " + clientId);
							return false;
						}
						response = getApiClient().handleProfileRegistration(request, apiEndPoint);
						if (response != null && response.optJSONObject("message") != null) {
							request.setAttribute(API_RESPONSE_CODE, response.optJSONObject("message").optString("code"));
							request.setAttribute(API_RESPONSE, response.optJSONObject("message").optString("description"));
						}
					} else if (ACTION_FORGOT_PASSWORD.equalsIgnoreCase(request.getParameter(PARAM_ACTION))) {
						apiEndPoint = resolveResourceServerServiceEndpoint(request, ENV_PROP_URL_FORGOT_PASSWORD, clientId);
						if (apiEndPoint == null) {
							LOGGER.info("No forgotPassword end point for clientId " + clientId);
							return false;
						}
						response = getApiClient().handleForgotPassword(request, apiEndPoint);
						if (response != null && !response.getBoolean("success")) {
							request.setAttribute(API_RESPONSE_CODE, response.optJSONObject("message").optString("code"));
							request.setAttribute(API_RESPONSE, response.optJSONObject("message").optString("description"));
							return false;
						} else {
							String profileID = response.optString("profile_id");
							expireExistingTokens(profileID);
						}
						return true;
					} else {
						apiEndPoint = resolveResourceServerServiceEndpoint(request, ENV_PROP_URL_LOGIN, clientId);
						if (apiEndPoint == null) {
							LOGGER.info("No authentication end point for clientId " + clientId);
							return false;
						}
						response = getApiClient().handleAuthentication(request, apiEndPoint);
						if (!response.getBoolean("success")) {
							request.setAttribute(API_RESPONSE_CODE, "500");
							request.setAttribute(API_RESPONSE, "Sorry, your email or password are not recognized. "
									+ "If you already have an account and have forgotten your password, please reset it. "
									+ "Please note: receiving emails or having placed orders in the past may not mean you have an account. "
									+ "Please create a new account if this is the case.");
						} else {
							String profileID = response.optString("profile_id");
							removeExistingTokens(profileID);
						}
					}
				}
				if (response != null && response.getBoolean("success")) {
					request.setAttribute(API_RESPONSE, getAuthenticatedData(response));
					return true;
				}
				return false;
			} catch (Exception e) {
				LOGGER.error("Error occurred while authenticating with API server" , e);
				return false;
			}
		}
		return canCommerce;
	}

	/**
	 * Remove existing tokens associated with the application when registered user logs in again. 
	 * Only one current token per Application per User. 
	 * As a result the user will be logged out of all other devices using that same application.
	 */
	private void removeExistingTokens(String profileID) {
		List<AccessToken> tokensToDelete = accessTokenRepository.findByResourceOwnerId(profileID);
		for (AccessToken tokenToDelete : tokensToDelete) {
			accessTokenRepository.delete(tokenToDelete);
		}
	}
	
	/**
	 * Expire all of the tokens for all applications when password is changed for the registered user. 
	 * User is required to re-login again and this will force all applications to update tokens.
	 */
	private void expireExistingTokens(String profileID) {
		List<AccessToken> tokensToExpire = accessTokenRepository.findByResourceOwnerId(profileID);
		for (AccessToken tokenToExpire : tokensToExpire) {
			tokenToExpire.setExpires(1);
			accessTokenRepository.save(tokensToExpire);
		}
	}

	@Override
	public void authenticate(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain,
			String authStateValue, String returnUri) throws IOException,
			ServletException {
		HttpSession session = request.getSession(false);
		AuthenticatedPrincipal principal = (AuthenticatedPrincipal) (session != null ? session
				.getAttribute(SESSION_IDENTIFIER) : null);
		if (request.getMethod().equals("POST")) {
			processForm(request);
			chain.doFilter(request, response);
		} else if (principal != null && !principal.isAlreadyLogged()) {
			// we still have the session
			principal.setAlreadyLogged(true);
			setAuthStateValue(request, authStateValue);
			setPrincipal(request, principal);
			chain.doFilter(request, response); 
		} else {
			String oldProfileID = request.getParameter(AuthenticationFilter.PARAM_OLD_PROFILE_ID);
			processInitial(request, response, returnUri, authStateValue, oldProfileID);
		}
	}

	private void processInitial(HttpServletRequest request, ServletResponse response, String returnUri,
			String authStateValue, String profileID) throws IOException, ServletException {
		request.setAttribute(AUTH_STATE, authStateValue);
		request.setAttribute("actionUri", returnUri);
		request.setAttribute(AuthenticationFilter.PARAM_OLD_PROFILE_ID, profileID);
		request.getRequestDispatcher("/WEB-INF/jsp/surfstitch/login_register.jsp").forward(request, response);
	}

	/*
	 * Get list of adminUserIds from apis property file
	 */
	
	private List<String> getAdminUserIds () {
		String adminUserIdsString = getEnv().getProperty(PROP_ADMIN_USER_IDS);
		if (adminUserIdsString == null || adminUserIdsString.isEmpty())
			return Collections.EMPTY_LIST;
		else
			return Arrays.asList(adminUserIdsString.split("\\s*,\\s*"));
	}

	/* (non-Javadoc)
	 * @see org.surfnet.oaaas.authentication.FormLoginAuthenticator#processForm(javax.servlet.http.HttpServletRequest)
	 * 
	 * This method sets the roles retrieved as part of the authentication call from APIClient#handleAuthentication in the 
	 * AuthenticatedPrincipal. 
	 * The AuthenticatedPrincipal is retrieved using request.getAttribute(PRINCIPAL). It is expected to be already set 
	 * in the request before the call reaches this method. It it doesn't, it's an error case scenario and is not handled 
	 * currently.
	 */
	@Override
	protected void processForm(HttpServletRequest request) {
		super.processForm(request);

		AuthenticatedPrincipal principal = (AuthenticatedPrincipal)request.getAttribute(PRINCIPAL);		
		JSONObject authenticatedData = (JSONObject) request.getAttribute(API_RESPONSE);
		
		if(authenticatedData==null){
			//--- This should not happen. If it does, it's an erroneous situation
			LOGGER.error("UNEXPECTED. We should not reach here. Roles response should not be null");
			return;
		}
		try {
			JSONArray roles = (JSONArray) authenticatedData.getJSONArray("roles");
			List<String> rolesList = new ArrayList<String>();
			for (int i = 0; i < roles.length(); i++) {
				rolesList.add((String)roles.get(i));
			}
			principal.setRoles(rolesList);
			principal.setName(authenticatedData.getString(PROFILE_NAME_IN_RESPONSE));
			principal.setAdminPrincipal(getAdminUserIds().contains(principal.getName()));
		} catch (JSONException e) {
			request.setAttribute(PRINCIPAL, null);
			LOGGER.error("Error while setting roles in Principal", e);
		}
	}

	/**
	 * This method adds roles to the passed {@link JSONObject} parameter. The roles are currently static "user" 
	 * and "guest" roles and gets added as {@link JSONArray}
	 *  
	 * @param jsonObject
	 * @throws JSONException
	 */
	private JSONObject getAuthenticatedData(JSONObject jsonObject) throws JSONException{
		JSONObject authenticatedData = new JSONObject();
		JSONArray roles = new JSONArray();
		roles.put("guest");
		roles.put("user");
		authenticatedData.putOpt("roles", roles);
		authenticatedData.put("name", jsonObject.get("profile_id"));
		return authenticatedData;
	}

	public OEAPIClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(OEAPIClient apiClient) {
		this.apiClient = apiClient;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}
}
