<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>Login</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/bootstrap.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/style.css" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/jquery.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/bootstrap.min.js"></script>
</head>

<body>
Login denied! Please try again.
	<p>
		<a onclick="history.go(-1);">Go back</a>
	</p>
</body>
</html>