/*
Local administration application
 */
INSERT INTO resourceserver (id, contactEmail,  contactName, resourceServerName, resourceServerKey, secret, owner, thumbNailUrl)
VALUES
	(99998, 'localadmin@example.com','local admin','Authorization Server Apis',
	'authorization-server-admin', 'cafebabe-cafe-babe-cafe-babecafebabe', 'admin', 'https://raw.github.com/OpenConextApps/apis/master/apis-images/surf-oauth.png');
INSERT INTO ResourceServer_scopes values (99998, 'read'),(99998, 'write') ;

INSERT INTO client (id, contactEmail, contactName, description, clientName, thumbNailUrl, resourceserver_id,
clientId, includePrincipal, allowedImplicitGrant)
VALUES
    (99998, 'client@coolapp.com', 'john.doe', 'Javascript application for authorization server administration',
    'Authorization Server Admin Client',
    'https://raw.github.com/OpenConextApps/apis/master/apis-images/surf-oauth-client.png', 99998,
    'authorization-server-admin-js-client', 1, 1);
INSERT INTO Client_scopes values (99998, 'read'), (99998, 'write');


/*
SSAPI Resource Server
 */

INSERT INTO resourceserver (id, contactEmail,  contactName, resourceServerName, resourceServerKey,secret, owner, thumbNailUrl )
VALUES
	(99999, 'ssapi-admin@gmail.com','ssapi-contact','ssapi','ssapi',
	'58b749f7-acb3-44b7-a38c-53d5ad740cf6','admin', 'https://raw.github.com/OpenConextApps/apis/master/apis-images/university.png' );
INSERT INTO Resourceserver_scopes values (99999, 'read'), (99999, 'write');

/*
Special client for SSAPI Resource Server, to be accessed by AuthServer
*/

INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, secret, skipConsent, thumbNailUrl,
					useRefreshTokens, resourceserver_id, allowedClientCredentials)
VALUES
    (88888, 'authserverclient', 'ssapi-admin@gmail.com', 'ssapi-contact', 'AuthServer->SSAPI client', 0,
    'authserverclient', 'd9dabeae-ce58-4cc1-94b9-17cbcc3add92', 0, 'https://raw.github.com/OpenConextApps/apis/master/apis-images/cool_app.png',
    0, 99999, 1);
INSERT INTO Client_scopes values (88888, 'read'), (88888, 'write');
INSERT INTO Client_redirectUris values (88888, 'http://localhost:8084/redirect');

/*
Example client 1, for guest users (allowedClientCredentials=true)
*/

INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, secret, skipConsent, thumbNailUrl,
					useRefreshTokens, resourceserver_id, allowedClientCredentials)
VALUES
    (99990, 'cool_app_id_1', 'client@coolapp.com', 'john.doe', 'Cool app for doing awesome things', 0,
    'cool_app_id_1', 'd9dabeae-ce58-4cc1-94b9-17cbcc666666', 0, 'https://raw.github.com/OpenConextApps/apis/master/apis-images/cool_app.png',
    0, 99999, 1);
INSERT INTO Client_scopes values (99990, 'read'),(99990, 'write');
INSERT INTO Client_redirectUris values (99990, 'http://localhost:8084/redirect');

/*
Example client 2, for regular users (implicit grant, w/o authCode flow)
*/

INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, skipConsent, thumbNailUrl,
					useRefreshTokens, resourceserver_id, allowedImplicitGrant)
VALUES
    (99991, 'cool_app_id_2', 'client@coolapp.com', 'john.doe', 'Cool app for doing awesome things', 0,
    'cool_app_id_2', 0, 'https://raw.github.com/OpenConextApps/apis/master/apis-images/cool_app.png',
    0, 99999, 1);
INSERT INTO Client_scopes values (99991, 'read'),(99991, 'write');
INSERT INTO Client_redirectUris values (99991, 'http://localhost:8084/redirect');


/*
Example client 3, for regular users (with authCode flow)
*/

INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, secret, skipConsent, thumbNailUrl,
					useRefreshTokens, resourceserver_id)
VALUES
    (99992, 'cool_app_id_3', 'client@coolapp.com', 'john.doe', 'Cool app for doing awesome things', 0,
    'cool_app_id_3', 'secret', 0, 'https://raw.github.com/OpenConextApps/apis/master/apis-images/cool_app.png',
    0, 99999);
INSERT INTO Client_scopes values (99992, 'read'),(99992, 'write');
INSERT INTO Client_redirectUris values (99992, 'http://localhost:8084/redirect');

